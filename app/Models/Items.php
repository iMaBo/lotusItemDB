<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Items extends Model
{
    use HasFactory, Notifiable;

    public static function search($search)
    {
        return empty($search) ? static::query() 
            : static::query()->where('name', 'like', '%'.$search.'%')
            ->orWhere('label', 'like', '%'.$search.'%');
    }
}
