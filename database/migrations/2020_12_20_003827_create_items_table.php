<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->varchar("name");
            $table->varchar("label");
            $table->integer("weight");
            $table->enum("type", ["weapon","item"]);
            $table->varchar("ammotype")->nullable(true);
            $table->varchar("image");
            $table->tinyInteger("unique");
            $table->tinyInteger("useable");
            $table->tinyInteger("shouldClose")->nullable(true);
            $table->varchar("description");
            $table->json("combinable")->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
