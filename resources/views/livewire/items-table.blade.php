<div>
    <div class="w-full flex pb-10">
        <div class="w-3/6 mx-1">
            <input wire:model.debounce.300ms="search" type="text" class="appearance-none block w-full text-gray-100 border rounded py-3 px-4 leading-tight focus:outline-none form-style"placeholder="Search item...">
        </div>
        <div class="w-1/6 relative mx-1">
            <select wire:model="orderBy" class="block appearance-none w-full border text-gray-100 py-3 px-4 pr-8 rounded leading-tight focus:outline-none form-style" id="grid-state">
                <option value="id">ID</option>
                <option value="name">Spawn Naam</option>
                <option value="label">Label</option>
                <option value="image">image</option>
            </select>
            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-100">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
            </div>
        </div>
        <div class="w-1/6 relative mx-1">
            <select wire:model="orderAsc" class="block appearance-none w-full text-gray-100 py-3 px-4 pr-8 rounded leading-tight focus:outline-none form-style" id="grid-state">
                <option value="1">Ascending</option>
                <option value="0">Descending</option>
            </select>
            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-100">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
            </div>
        </div>
        <div class="w-1/6 relative mx-1">
            <select wire:model="perPage" class="block appearance-none w-full text-gray-100 py-3 px-4 pr-8 rounded leading-tight focus:outline-none form-style" id="grid-state">
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
            </select>
            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-100">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
            </div>
        </div>
    </div>
    <table class="table-auto w-full mb-6 border-none table-style">
        <thead>
            <tr>
                <th class="px-4 py-2">ID</th>
                <th class="px-4 py-2">Spawn Naam</th>
                <th class="px-4 py-2">Label</th>
                <th class="px-4 py-2">image</th>
            </tr>
        </thead>
        <tbody>
            @foreach($items as $item)
                <tr class="row-style" id="{{ $item->id }}"">
                    <td class="px-4 py-2 text-center">{{ $item->id }}</td>
                    <td class="px-4 py-2" id="{{ $item->name }}">{{ $item->name }}</td>
                    <td class="px-4 py-2">{{ $item->label }}</td>
                    <td class="px-4 py-2"><img src="./img/{{ $item->image }}" alt="" srcset="" width="30px"></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {!! $items->links() !!}
</div>
