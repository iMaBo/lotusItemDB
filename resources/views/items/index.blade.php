<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Items</title>

        <!-- TailwindCSS -->
        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: 'Nunito';
                background-color: #3e4652;
            }
            .form-style {
                background-color: #505967;
                border-color: #464e5b;
            }
            .form-style:focus {
                background-color: #5f6775;
                border-color:rgb(76, 83, 95);
            }

            .table-style thead {
                background-color: #505967;
                color: #FFF;
            }
            .row-style:nth-child(even) {
                background-color: #505967;
                color: #FFF;
            }
            .row-style:nth-child(odd) {
                background-color: #454d58;
                color: #FFF;

            }
        </style>

        @livewireStyles
    </head>
    <body>
        <div class="container mx-auto">
            <h1 class="text-3xl text-center my-10" style="color: #b9c3ce;">Items DB</h1>
            <livewire:items-table>
        </div>

        @livewireScripts
        <script>
            function CopyToClipboard(ids) {
                var spawnName = document.getElementById(ids);
                var r = document.createRange();
                console.log(spawnName);
                r.selectNode(document.getElementById(ids));
                window.getSelection().removeAllRanges();
                window.getSelection().addRange(r)
                document.execCommand('copy');
                window.getSelection().removeAllRanges();
            }
        </script>    
    </body>
</html>